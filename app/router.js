import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('clients');

  this.route('client', { path: "/client/:client_id" }, function() {
    this.route('merchants');
    this.route('merchant', { path: "/merchant/:merchant_id"}, function(){
      this.route('business');
      this.route('onboarding');
      this.route('location-profiles');
      this.route('practitioner-profiles');
    });
  });

  this.route('coming-soon');
});

export default Router;
