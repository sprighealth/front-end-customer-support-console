import DS from 'ember-data';

export default DS.Model.extend({
  onboardingRequests: DS.hasMany('onboarding-request'),
  contactAddresses: DS.hasMany('contact-address'),
  clients: DS.belongsTo('client'),
  name: DS.attr('string'),
  dba_name: DS.attr('string'),
  organization_npi: DS.attr('string'),
  efi: DS.attr('string'),
  tax_id: DS.attr('string'),
  status: DS.attr('string'),
  date_of_change: DS.attr('string'),
  action_by: DS.attr('string')
});
