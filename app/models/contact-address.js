import DS from 'ember-data';

export default DS.Model.extend({
  merchant: DS.belongsTo('merchant'),

  city: DS.attr('String'),
  contactType: DS.attr('String'),
  email: DS.attr('String'),
  fax: DS.attr('String'),
  firstName: DS.attr('String'),
  lastName: DS.attr('String'),
  phone: DS.attr('String'),
  state: DS.attr('String'),
  street1: DS.attr('String'),
  street2: DS.attr('String'),
  zip: DS.attr('String')
});
