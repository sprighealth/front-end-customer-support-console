import DS from 'ember-data';

export default DS.Model.extend({
  merchant: DS.belongsTo('merchant'),
  enrollmentStatus: DS.attr('string'),
  inviteUrl: DS.attr('string'),
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  email: DS.attr('string')
});
