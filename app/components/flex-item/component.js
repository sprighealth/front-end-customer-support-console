import Ember from 'ember';

export default Ember.Component.extend({
  didReceiveAttrs(){
    this._super(...arguments);
    const flexLevel = this.get('flex');
    switch(flexLevel){
      case 1:
        this.set('flexOne', true);
        break;
      case 2:
        this.set('flexTwo', true);
        break;
      case 3:
        this.set('flexThree', true);
        break;
      case 4:
        this.set('flexFour', true);
        break;
    }

    const orderLevel = this.get('order');
    switch(orderLevel){
      case 1:
        this.set('orderOne', true);
        break;
      case 2:
        this.set('orderTwo', true);
        break;
      case 3:
        this.set('orderThree', true);
        break;
      case 4:
        this.set('orderFour', true);
        break;
    }

  },

  classNameBindings: [
    'flexOne:flex-1', 
    'flexTwo:flex-2', 
    'flexThree:flex-3', 
    'flexFour:flex-4',
    'orderOne:order-1', 
    'orderTwo:order-2', 
    'orderThree:order-3', 
    'orderFour:order-4'
  ]
});
