import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    submitIt: function() {
      var params = {
        legalBusinessName: this.get('legalBusinessName'),
        zip: this.get('zip'),
        address: this.get('address'),
        taxId: this.get('taxId')
      };
      this.get('searchHandler')(params);
    }
  }
});
