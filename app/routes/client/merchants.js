import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    const client_id = this.paramsFor('client')['client_id'];
    return this.get('store').query('merchant', {client_id: client_id});
  },
});
